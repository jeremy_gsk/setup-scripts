# ======================================================================

# start by first backing up defaults database into tmp
defaults read > "/tmp/$USER.defaults"

echo "Starting setting up of MacOS preferences..."
# =======================================================================

# ------
# Manual
# -------

# Some things can only be adjusted manually it seems, need to double check...

# Sound -> "Show sound in menu bar" set to Always
# Control Center -> Battery -> Turn on "Show in Menu Bar" and "Show battery percentage" (seems defaults
#   doesn't apply..)
# Desktop & Dock -> Desktop & Stage Manager -> Click wallpaper to reveal desktop -> Set this to "Only in Stage
#   Manager"
# Displays -> Turn off "Automatically adjust brightness"
# Lock Screen -> Require password after screen saver begins or display is turned off -> Set to "Immediately"
# Keyboard -> Add "Pinyin - Simplified" input source
# Keyboard -> Shortcuts -> Spotlight -> Turn off all options (will use Raycast later)
# Keyboard -> Modifier keys -> Change "Capslock" to "Control"
# Trackpad -> More Gestures -> Turn on "App Expose" (swipe down with 3 fingers)

# Finder -> Preferences (of Finder.app) -> General -> Change "New Finder window shows" to $HOME instead of Recents
# Finder -> Preferences (of Finder.app) -> Sidebar -> Favourites -> Untick Recents, Airdrop, Tick the $HOME folder
# Finder -> View -> Click "Show Path Bar" (breadcrumb view)


# ------
# General
# -------

# Accent colour
# - Blue
# defaults delete "Apple Global Domain" AppleAccentColor > /dev/null 2>&1
# - Red
defaults write "Apple Global Domain" AppleAccentColor -int 0

# Highlight colour
# - Blue
# defaults delete "Apple Global Domain" AppleHighlightColor > /dev/null 2>&1
# - Red
defaults write "Apple Global Domain" AppleHighlightColor -string "1.000000 0.733333 0.721569 Red"


# -------
# Battery
# -------

# Show battery life percentage in menu bar.
defaults -currentHost write com.apple.controlcenter BatteryShowPercentage 1

# Turn off "Wake for network access"
sudo pmset -a womp 0

# Turn display off after...
sudo pmset -b displaysleep 3


# -----
# Clock
# -----

# Date format as 'Sat 9 Jan  11:44'
defaults write com.apple.menuextra.clock DateFormat -string "EEE d MMM  HH:mm"


# ----
# Dock
# ----

# Automatically hide and show the Dock.
defaults write com.apple.dock autohide -bool true

# Position on screen.
defaults write com.apple.dock orientation -string "left"

# Size
defaults write com.apple.dock tilesize -int 60

# Magnification of dock tiles
defaults write com.apple.dock magnification -int 1
defaults write com.apple.dock largesize -int 77

# Show indicators for open applications
defaults write com.apple.dock show-process-indicators -bool true


# ------
# Finder
# ------

# Show all filename extensions
defaults write NSGlobalDomain AppleShowAllExtensions -bool true

# Avoid creating .DS_STORE files on network or USB volumes
defaults write com.apple.desktopservices DSDontWriteNetworkStores -bool true
defaults write com.apple.desktopservices DSDontWriteUSBStores -bool true

# When performing a search, search the current folder by default
defaults write com.apple.finder FXDefaultSearchScope -string "SCcf"

# Use list view in all Finder windows by default
# Four-letter codes for the other view modes: `icnv`, `clmv`, `Flwv`
defaults write com.apple.Finder FXPreferredViewStyle -string "Nlsv"


# --------
# Keyboard
# --------

# Adjust key repeat to the fastest possible.
defaults write NSGlobalDomain KeyRepeat -int 2

# Stop automatic substitution of double space -> period
defaults write "Apple Global Domain" NSAutomaticPeriodSubstitutionEnabled -bool false

# Turn off auto-correct
defaults write NSGlobalDomain NSAutomaticSpellingCorrectionEnabled -bool false


# ----
# Misc
# ----

# GPG >> Disable option to store password in macOS keychain.
# source: https://gpgtools.tenderapp.com/kb/gpg-mail-faq/gpg-mail-hidden-settings
defaults write org.gpgtools.common DisableKeychain -bool yes


# =======================================================================
# clear cache
for app in \
    "cfprefsd" "Dock" "Finder"
do
    killall ${app} > /dev/null 2>&1
done
echo "Done. Note that some of these changes require a logout/restart to take effect."
echo "A backup file has been generated into /tmp/$USER.defaults"


# =======================================================================
# To be refactored out into its own setup script
echo "Installing xcode-select CLI tools..."
xcode-select --install 2>/dev/null
