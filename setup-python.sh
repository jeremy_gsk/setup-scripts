#!/usr/bin/env bash

# ::WARNING:: This script is currently incomplete.
# NOTE: Consider To convert into a sequential style, ask the user whether they want to xxx at every step
#
# Not meant for upgrading (e.g. pipx packages). Do that on your own outside of this script.
# This script is only meant for initial setting up on a new machine (bootstrapping).
#
# Expected usage:
#     ./setup-python.sh       --> prints help, and available py-related installations
#     ./setup-python.sh all   --> installs everything
#     ./setup-python.sh pipx  --> install only pipx related

_red=$(tput setaf 9 2>/dev/null)
_yellow=$(tput setaf 228 2>/dev/null)
_resetc=$(tput sgr0 2>/dev/null)

_helptext () {
    cat <<HELP_USAGE
Usage:
$(basename "$0") <prog>

where:
    <prog>  One of the following: all, pyenv, pipx
HELP_USAGE
}


_install_pyenv() {
    if command -v pyenv &>/dev/null ; then
        echo "🔸 ${_yellow}You seem to already have 'pyenv' installed,"\
            "skipping pyenv installation...${_resetc}"
        return
    fi

    echo "🔸 ${_yellow}Initiating pyenv installation"\
        "(📕 https://github.com/pyenv/pyenv-installer)...${_resetc}"
    local pt="${_yellow}Please ensure you read the installation script "
    pt+="carefully first before proceeding. Press any key to continue${_resetc}: "
    read -n 1 -p "$pt" _

    curl https://pyenv.run | bash
    echo "🟢 ${_yellow}pyenv installation complete. You can start installing python"\
        "versions using 'pyenv install'.${_resetc}"
}

_install_pipx () {
    if ! command -v pipx &>/dev/null ; then
        echo "🔸 ${_yellow}pipx not found in global py environment, please"\
            "install it according to the docs manually first.${_resetc}"\
            "(📕 https://pypa.github.io/pipx/)"
        return
    fi

    local SETUPPY_PIPX_PKGS=(
        black
        uv             # https://github.com/astral-sh/uv
        ruff           # https://github.com/astral-sh/ruff
        # yt-dlp         # https://github.com/yt-dlp/yt-dlp
        beets          # https://github.com/beetbox/beets
        csvkit         # https://github.com/wireservice/csvkit
        httpie         # https://github.com/httpie/httpie
        magic-wormhole # https://github.com/magic-wormhole/magic-wormhole
        pre-commit     # https://github.com/pre-commit/pre-commit
        diceware       # https://github.com/ulif/diceware
        nb-clean       # https://github.com/srstevenson/nb-clean
        nbqa           # https://github.com/nbQA-dev/nbQA
    )
    echo "🔸 ${_yellow}Initiating pipx packages installation...${_resetc}"
    echo "${SETUPPY_PIPX_PKGS[@]}" | xargs -n1 pipx install

    pipx inject nbqa \
        black \
        ruff

    # finally, install personal scripts
    pipx install git+ssh://git@github.com/thatlittleboy/lgtm-db
    # pipx install git+ssh://git@github.com/thatlittleboy/npv

    echo
    pipx list --include-injected  # reveals the actual path + installed apps + injected packages
    echo "🟢 ${_yellow}pipx installation complete.${_resetc}"
}


# --- Main ---
if [[ $# -gt 1 ]] ; then
    echo "${_red}ERROR: only allow one argument.${_resetc}"
    echo
    _helptext
    exit 1
fi

case "$1" in
    "" )
        # prints help / usage text if no args supplied
        _helptext
        exit 0
    ;;

    all )
        # installs everything (useful for a fresh installation only)
        # installation order is important here
        _install_pyenv
        sleep 2
        _install_pipx
    ;;

    pyenv )
        _install_pyenv
    ;;

    pipx )
        _install_pipx
    ;;

    * )
        echo "${_red}ERROR: prog" "$1" "not recognized.${_resetc}"
        echo
        _helptext
        exit 1
    ;;
esac


# --- Clean up ---
unset _red _yellow _resetc
unset _helptext
unset _install_pyenv _install_pipx


read -p "🎉 Hooray! All done! Restart shell? [y/N]  " REPLY
case "${REPLY:0:1}" in
    y|Y )
        exec $SHELL -l
    ;;
esac
