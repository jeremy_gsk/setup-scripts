# Setup scripts

These scripts are meant to setup specific settings.
It should be the first thing being executed when initializing a new machine.
Can consider to eventually expand this to include an `init.sh` script.

```bash
source ~/.config/setup-scripts/xxx.sh
```

Other ideas for setup script include `setup-vscode.sh` (install all required VS Code extensions), etc.

See https://github.com/pawelgrzybek/dotfiles

## Copy in local machine

Source the following files directly from the Internet:

```bash
source <(curl -s https://gitlab.com/jeremy_gsk/setup-scripts/-/raw/main/setup-macos.sh)
bash -c "$(curl -fsSL https://gitlab.com/jeremy_gsk/setup-scripts/-/raw/main/setup-brew.sh)"
```

Git is installed from `setup-brew.sh` above.
You just need to set up your .ssh keys, then clone this repository into `$HOME/.config`.
The other scripts need to be executed instead of sourced, for example:

```bash
chmod u+x setup-python.sh
./setup-python.sh all
```
