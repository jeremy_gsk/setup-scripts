#!/usr/bin/env bash

# --- Initializations ---
_yellow=$(tput setaf 228 2>/dev/null)
_resetc=$(tput sgr0 2>/dev/null)

ask() {
    local prompt_text="$1 (y/[n]) "
    while true; do
        echo -n "$prompt_text"
        read REPLY
        REPLY=${REPLY:-"n"}
        if [[ $REPLY =~ ^[Yy]$ ]]; then
            return 1
        elif [[ $REPLY =~ ^[Nn]$ ]]; then
            return 0
        fi
    done
}

if ! command -v brew &> /dev/null
then
    echo "🔸 ${_yellow}Installing brew...${_resetc}"\
        "(📕 https://brew.sh/)"
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
else
    echo "🔸 ${_yellow}brew already installed. Skipping brew installation...${_resetc}"
fi

brew update


# --- Install ---
SETUPBREW_PACKAGES=(
    #
    # -- common utility tools --
    #
    alt-tab         # https://github.com/lwouis/alt-tab-macos
    bat             # https://github.com/sharkdp/bat
    cmus            # https://github.com/cmus/cmus
    dua-cli         # https://github.com/Byron/dua-cli
    fd              # https://github.com/sharkdp/fd
    fzf             # https://github.com/junegunn/fzf
    git-delta       # https://github.com/dandavison/delta
    mpv             # https://github.com/mpv-player/mpv
    neovim          # https://github.com/neovim/neovim
    rclone          # https://github.com/rclone/rclone
    shellcheck      # https://github.com/koalaman/shellcheck
    tealdeer        # https://github.com/dbrgn/tealdeer
    #
    # -- for getting a more recent version rather than using the built-ins --
    #
    git
    less  # the default macos less version=487 doesn't play well with delta
    vim
    zsh
    #
    # -- others/gnu utilities --
    #
    gnupg2
    imagemagick
    jq
    openssl  # pyenv dependency
    pinentry-mac
    readline  # pyenv dependency
    svn  # font-ubuntu-mono dependency
    telnet
    tree
    xz  # pyenv dependency
    wget
)
echo "🔸 ${_yellow}Installing core packages round 1...${_resetc}"
brew install "${SETUPBREW_PACKAGES[@]}"

SETUPBREW_PACKAGES=(
    gopass          # https://github.com/gopasspw/gopass
    gopass-jsonapi  # https://github.com/gopasspw/gopass-jsonapi
    #
    # -- optional --
    #
    # google-cloud-sdk
    # kubectl
    # kubeseal
    # openconnect
)
echo "🔸 ${_yellow}Installing core packages round 2...${_resetc}"
brew install "${SETUPBREW_PACKAGES[@]}"


SETUPBREW_CASKS=(
    adobe-acrobat-reader
    alt-tab
    docker
    firefox
    flameshot
    joplin
    # kitty  # install via binary: https://sw.kovidgoyal.net/kitty/binary/
    onedrive
    raycast
    rectangle
    telegram
    visual-studio-code
    # bitwarden  # install manually from app store
)
echo "🔸 ${_yellow}Installing cask apps (GUIs)...${_resetc}"
brew install --cask "${SETUPBREW_CASKS[@]}"


# some useful tools to interactively view fonts
# - https://devfonts.gafi.dev/
SETUPBREW_CASKFONTS=(
    font-hack
    font-symbols-only-nerd-font

    # font-hack-nerd-font
    font-ubuntu-mono-nerd-font
    font-source-code-pro
)
echo "🔸 ${_yellow}Installing cask fonts...${_resetc}"
brew install --cask "${SETUPBREW_CASKFONTS[@]}"


SETUPBREW_MANUALPKG=(
    microsoft_excel
    microsoft_powerpoint
    microsoft_onenote
    kap  # under test
    bitwarden
    raivo  # under test
    lark  # for work
    wechat
)
echo "🔸 ${_yellow}Install these apps manually (list below), from the app"\
    "store or otherwise${_resetc}"
echo "🔸   " "${SETUPBREW_MANUALPKG[@]}"
echo
sleep 3


# --- Clean up ---
brew analytics off
brew cleanup -s
rm -rf "$(brew --cache)"

# Not necessary if user is executing the script rather than sourcing it, but just
# to be safe.
unset SETUPBREW_PACKAGES
unset SETUPBREW_CASKS
unset SETUPBREW_CASKFONTS
unset SETUPBREW_MANUALPKG

ask "Done with brew... change shell to zsh?"
if [ $? -eq 1 ]; then
    brew_zsh=$(brew --prefix)/bin/zsh
    if ! grep -q "${brew_zsh}" /etc/shells; then
        sudo sh -c "echo \"${brew_zsh}\" >> /etc/shells"
    fi
    chsh -s "${brew_zsh}"
    echo "Please logout and restart shell to check if your default shell has changed."
else
    echo "Shell not changed. You may change it yourself manually"\
        "using \`chsh -s \$(brew --prefix)/bin/zsh\`"
fi

echo "🟢 ${_yellow}Brew installation and setup complete.${_resetc}"
unset _yellow _resetc
