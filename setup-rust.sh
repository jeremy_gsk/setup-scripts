#!/usr/bin/env bash

# ::WARNING:: This script is currently incomplete.
# NOTE: Consider to convert into a sequential style, ask the user whether they want to xxx at every step
# TODO: rustup toolchain install nightly
# TODO: rustup component add rust-analyzer (components like cargo, docs, rustfmt already come with the
#       default profile when installing.)
#
# Expected usage:
#     ./setup-rust.sh       --> prints help, and available rust-related installations
#     ./setup-rust.sh all   --> installs everything

_red=$(tput setaf 9 2>/dev/null)
_yellow=$(tput setaf 228 2>/dev/null)
_resetc=$(tput sgr0 2>/dev/null)

_helptext () {
    cat <<HELP_USAGE
Usage:
$(basename "$0") <prog>

where:
    <prog>  One of the following: all,
HELP_USAGE
}


_install_rustup() {
    if command -v rustup &>/dev/null ; then
        echo "🔸 ${_yellow}You seem to already have 'rustup' installed,"\
            "skipping rustup installation...${_resetc}"
        return
    fi

    echo "🔸 ${_yellow}Initiating rustup installation"\
        "(📕 https://rust-lang.github.io/rustup/installation/other.html)...${_resetc}"
    local pt="${_yellow}Please ensure you read the installation script "
    pt+="carefully first before proceeding. Press any key to continue${_resetc}: "
    read -n 1 -p "$pt" _

    # note: you can run --help to view the rustup-init available commands
    # The following script runs in a subshell with a confirmation prompt.
    # You can let it modify PATH, we should be overriding it later.
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- --verbose
    echo "🟢 ${_yellow}rustup installation complete.${_resetc}"
}


# --- Main ---
if [[ $# -gt 1 ]] ; then
    echo "${_red}ERROR: only allow one argument.${_resetc}"
    echo
    _helptext
    exit 1
fi

case "$1" in
    "" )
        # prints help / usage text if no args supplied
        _helptext
        exit 0
    ;;

    all )
        # installs everything (useful for a fresh installation only)
        # installation order is important here
        _install_rustup
        sleep 2
    ;;

    * )
        echo "${_red}ERROR: prog" "$1" "not recognized.${_resetc}"
        echo
        _helptext
        exit 1
    ;;
esac


# --- Clean up ---
unset _red _yellow _resetc
unset _helptext
unset _install_rustup


read -p "🎉 Hooray! All done! Restart shell? [y/N]  " REPLY
case "${REPLY:0:1}" in
    y|Y )
        exec $SHELL -l
    ;;
esac
